import { AppRoutes } from './app-routes.const';


export const AMS_WAD_LEFT_NAVIGATION = [
    {
        title: 'Home',
        url: '/' + AppRoutes.Public,
        icon: 'home',
        authorised: false
    },
    {
        title: 'Dashboard',
        url: '/' + AppRoutes.Public,
        icon: 'dashboard',
        authorised: true
    },
    {
        title: 'Airports',
        url: '/' + AppRoutes.Airports,
        icon: 'local_airport',
        authorised: true
    },
    {
        title: 'AMS Reporter',
        url: '/' + AppRoutes.Newspaper,
        icon: 'wallpaper',
        authorised: false
    },
];
