export const AmsApplications = {
    WAD: {
        title: 'World Airport Database',
        abbreviation: 'WAD',
        // tslint:disable-next-line:max-line-length
        description: 'AMS World Airport Database is a web information system that contains data for almost every airport in the world, including airport codes, abbreviations, runway lengths and other airport details.',
        // tslint:disable-next-line:max-line-length
        note: 'AMS World Airport Database is a completely fictitious world, and any resemblance to real world airports, airport names, city names, country names, or otherwise, is completely coincidental.',
    },
    AMS: {
        title: 'World Airport Database',
        abbreviation: 'WAD',

        description: 'AMS World Airport Database is a web information ' +
            'system that contains data for almost every airport in the world, ' +
            'including airport codes, abbreviations, runway lengths and other airport details.',

        note: 'AMS World Airport Database is a completely fictitious world, ' +
            'and any resemblance to real world airports, airport names, city names, ' +
            'country names, or otherwise, is completely coincidental.',
    }



};



