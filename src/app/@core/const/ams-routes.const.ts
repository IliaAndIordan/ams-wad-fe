export const AmsRoutes = {
    Root: '',
    Public: 'public',
    About: 'about',
    Login: 'login',
    Dashboard: 'dashboard',
    NotAuthorized: 'not-authorized',
    UserProfile: 'user-profile',
    Vehicles: 'vehicles',
    Vehicle: 'vehicle',
};
