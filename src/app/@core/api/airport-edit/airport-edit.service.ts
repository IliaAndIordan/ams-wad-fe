import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
// Services
import { ServiceNames } from '../api.constants';
import { ApiHelper } from '../api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
import { TokenService } from '../../auth/token.service';
import { ToastrService } from 'ngx-toastr';
import { ResponseAirportCountryStateModel } from './dto';

const KEY_SELECTED_COUNTRY_ID = 'KEY_SELECTED_COUNTRY_ID';
const KEY_SELECTED_STATE_ID = 'KEY_SELECTED_STATE_ID';
const KEY_SELECTED_AP_ID = 'KEY_SELECTED_AP_ID';


@Injectable()
export class AirportEditService {

    constructor(private apiHelper: ApiHelper,
        private http: HttpClient,
        private toastr: ToastrService,
        private currUserService: CurrentUserService,
        private tokenService: TokenService) { }

    //#region URL Methods

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.airportsEdit);
    }

    //#endregion

    //#region Local Storige

    public set countryId(value: number) {
        // console.log('countryId: ', value);
        if (value) {
            localStorage.setItem(KEY_SELECTED_COUNTRY_ID, JSON.stringify(value));
        } else {
            localStorage.removeItem(KEY_SELECTED_COUNTRY_ID);
        }
    }

    public get countryId(): number {
        let countryId: number;
        const valueStr = localStorage.getItem(KEY_SELECTED_COUNTRY_ID);
        if (valueStr) {
            countryId = JSON.parse(valueStr);
        }
        return countryId;
    }

    public set stateId(value: number) {
        if (value) {
            localStorage.setItem(KEY_SELECTED_STATE_ID, JSON.stringify(value));
        } else {
            localStorage.removeItem(KEY_SELECTED_STATE_ID);
        }
    }

    public get stateId(): number {
        let value: number;
        const valueStr = localStorage.getItem(KEY_SELECTED_STATE_ID);
        if (valueStr) {
            value = JSON.parse(valueStr);
        }
        return value;
    }

    public set airportId(value: number) {
        if (value) {
            localStorage.setItem(KEY_SELECTED_AP_ID, JSON.stringify(value));
        } else {
            localStorage.removeItem(KEY_SELECTED_AP_ID);
        }
    }

    public get airportId(): number {
        let value: number;
        const valueStr = localStorage.getItem(KEY_SELECTED_AP_ID);
        if (valueStr) {
            value = JSON.parse(valueStr);
        }
        return value;
    }

    //#endregion

    //#region Country State Status Airport Count

    public getCountryStateStatusAirports(): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/countbycountry/', null, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`CountryStateStatusAirports fetched.`); }),
                catchError(this.handleError('getCountryStateStatusAirports', []))
            );
    }

    public getAirportsByCountryState(countryId: number, stateId: number): Observable<any> {
        this.countryId = countryId;
        this.stateId = stateId;
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/countrystate/',
            { country_id: countryId, state_id: stateId },
            { headers: hdrs })
            .pipe(
                tap(res => this.log(`getAirportsByCountryState fetched.`)),
                catchError(this.handleError('getAirportsByCountryState', []))
            );
    }

    //#endregion

    //#region Base

    /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            // this.toastr.error(`${operation} failed: ${error.message}`, 'WAD Service');

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Service message  */
    private log(message: string) {
        console.log('WAD Service', message);
        // this.toastr.info(message, 'WAD Service');
    }

    //#endregion
}
