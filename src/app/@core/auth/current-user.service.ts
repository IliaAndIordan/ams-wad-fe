import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Subject } from 'rxjs/Subject';

// Models
import { UserModel } from './api/dto';
import { AmsStorageKeysLocal } from '../const/ams-storage-key.const';
import { LocalUserSettings } from './local-user-settings';
import { TokenService } from './token.service';
import { GravatarService } from '@infinitycube/gravatar';
import { AuthService } from './api/auth.service';

@Injectable()
export class CurrentUserService {

    constructor(private tockenService: TokenService,
        private gravatarService: GravatarService) { }
    //#region User

    /**
     * User Changed subject
     * subscribe to it if you want to be notified when changes appear
     */
    public UserChanged = new Subject<UserModel>();


    public get user(): UserModel {
        const userStr = sessionStorage.getItem(AmsStorageKeysLocal.CurrentUser);
        let userObj = null;
        if (userStr) {
            userObj = Object.assign(new UserModel(), JSON.parse(userStr));
        }
        return userObj;
    }

    public set user(userObj: UserModel) {
        if (userObj) {
            sessionStorage.setItem(AmsStorageKeysLocal.CurrentUser, JSON.stringify(userObj));
        } else {
            sessionStorage.removeItem(AmsStorageKeysLocal.CurrentUser);
        }
        this.UserChanged.next(userObj);
    }

    public get isLogged(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        if (retValue) {
            // console.log('isLogged user: ', userObj);
        }
        // console.log('isLogged: ' + retValue);
        return retValue;
    }

    public get isAuthenticated(): boolean {
        // console.log('isAuthenticated: ');
        return this.isLogged;
    }

    public get avatarUrl(): string {
        let retValue = './../../../assets/images/common/iziordanov_sd_128_bw.png';
        const userObj: UserModel = this.user;

        if (userObj && userObj.eMail) {
            const gravatarImgUrl = this.gravatarService.url(userObj.eMail, 128, 'identicon');
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    //#endregion

    //#region Local User Settings

    // tslint:disable-next-line:member-ordering
    public LocalSettingsChanged = new Subject<LocalUserSettings>();

    public get localSettings(): LocalUserSettings {
        const userStr = sessionStorage.getItem(AmsStorageKeysLocal.LocalUserSettings);
        let userObj = null;
        if (userStr) {
            userObj = Object.assign(new LocalUserSettings(), JSON.parse(userStr));
        } else {
            userObj = new LocalUserSettings();
            userObj.snavLeftWidth = 4;
            sessionStorage.setItem(AmsStorageKeysLocal.LocalUserSettings, JSON.stringify(userObj));
        }
        return userObj;
    }

    public set localSettings(userObj: LocalUserSettings) {
        if (userObj) {
            sessionStorage.setItem(AmsStorageKeysLocal.LocalUserSettings, JSON.stringify(userObj));
        } else {
            sessionStorage.removeItem(AmsStorageKeysLocal.LocalUserSettings);
        }
        this.LocalSettingsChanged.next(userObj);
    }

    //#endregion

}
