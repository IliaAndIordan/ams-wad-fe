import { NgModule } from '@angular/core';
import { RouterModule, Routes, provideRoutes } from '@angular/router';
// Constants
import { AppRoutes } from './@core/const/app-routes.const';
// ---Components
import { PreloadSelectedModuleList } from './@core/preload-strategy';
import { AppComponent } from './app.component';

export const routes: Routes = [
  { path: AppRoutes.Root, pathMatch: 'full', redirectTo: AppRoutes.Public },
  { path: AppRoutes.Public, loadChildren: './public/public.module#PublicModule', data: { preload: true } },
  { path: AppRoutes.Newspaper, loadChildren: './newspaper/newspaper.module#NewspaperModule', data: { preload: true } },
  { path: AppRoutes.Airports, loadChildren: './airports/airports.module#AirportsModule', data: { preload: true } },


];

@NgModule({

  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadSelectedModuleList, enableTracing: false })
  ],
  exports: [RouterModule],
  providers: [PreloadSelectedModuleList]

})
export class AppRoutingModule { }

export const routableComponents = [];

