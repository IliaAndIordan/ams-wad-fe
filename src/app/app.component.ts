import { Component, ViewChild, OnInit } from '@angular/core';
import { ChangeDetectorRef, OnDestroy } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatSidenav, MatList, MatDialog } from '@angular/material';
import { AirportEditService } from './@core/api/airport-edit/airport-edit.service';
import { ResponseCountryStateStatusAirport } from './@core/api/airport-edit/dto';
import { CurrentUserService } from './@core/auth/current-user.service';
import { LoginModalDialog } from './@share/modal/login/login-modal.dialog';
import { UserModel } from './@core/auth/api/dto';
import { Router } from '@angular/router';
import { AppRoutes } from './@core/const/app-routes.const';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {
  title = 'ams-wad-fe';

  @ViewChild('snavl') snavl: MatSidenav;
  @ViewChild('snavr') snavr: MatSidenav;
  //#region SideNav Laft
  snavLeftKeepWhide = false;
  snavLeftWidth = 4;
  sidenavLeftmode = new FormControl('over');

  //#endregion

  shouldRun = true;
  mobileQuery: MediaQueryList;

  fillerNav = Array.from({ length: 50 }, (_, i) => `Nav Item ${i + 1}`);

  fillerContent = Array.from({ length: 50 }, () =>
    `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
       labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
       laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
       voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
       cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`);

  private _mobileQueryListener: () => void;

  constructor(
    private router: Router,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public dialogService: MatDialog,
    private currUserService: CurrentUserService,
    private airportEditService: AirportEditService) {

    this.snavLeftWidth = currUserService.localSettings.snavLeftWidth;

    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.getCountryStateStatusAirports();

    this.currUserService.UserChanged.subscribe((user: UserModel) => {
      this.onUserChanged();
    });
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngOnInit(): void {

  }

  onUserChanged() {
    if (this.currUserService.isAuthenticated) {
      this.router.navigate(['/' + AppRoutes.Dashboard]);
    } else {
      this.router.navigate(['/' + AppRoutes.Newspaper]);
    }
  }
  //#region  Test service

  getCountryStateStatusAirports(): void {
    // this.spinnerService.display(true);

    this.airportEditService.getCountryStateStatusAirports()
      .subscribe(
        (result: ResponseCountryStateStatusAirport) => this.processData(result),
        error => {
          // this.toastr.error(error);
        });

  }

  processData(data): void {
    console.log('processData', data);
  }

  //#endregion

  //#region Login/User Profile

  public onProfileClick(): void {
    if (this.currUserService.isAuthenticated) {
      console.log('Show user profile...');
    } else {
      this.loginDialogShow();
    }
  }


  public loginDialogShow(): void {
    const dialogRef = this.dialogService.open(LoginModalDialog, {
      width: '500px',
      height: '320px',
      data: { email: null, password: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      // this.password = result.password;
    });
  }

  //#endregion

  //#region SideNav Laft

  snavLeftToggle() {
    console.log('mobileQuery.matches: ' + this.mobileQuery.matches ? 'over' : 'side');
    let open = true;
    if (this.snavl.opened) {

      if (this.snavLeftKeepWhide) {
        this.snavLeftKeepWhide = false;
        open = false;
        if (this.snavLeftWidth === 15) {
          this.snavLeftWidth = 4;
        } else {
          this.snavLeftWidth = 4;
          // this.snavl.open();
        }

      } else {
        this.snavLeftKeepWhide = true;
        this.snavLeftWidth = 15;

        // this.snavl.mode = this.mobileQuery.matches ? 'over' : 'side';
        open = true;
      }
    } else {
      this.snavLeftKeepWhide = false;
      this.snavLeftWidth = 4;
      // this.snavl.toggle();
    }

    // tslint:disable-next-line:prefer-const
    let settinsgs = this.currUserService.localSettings;
    settinsgs.snavLeftWidth = this.snavLeftWidth;
    this.currUserService.localSettings = settinsgs;

    console.log('open: ' + open ? 'open' : 'close');
    open ? this.snavl.open() : this.snavl.close();
  }

  snavRightToggle() {
    this.snavr.toggle();
  }

  increase() {
    if (this.snavLeftWidth < 15) {
      // this.snavLeftWidth = 15;
    }

  }
  decrease() {
    if (this.currUserService.localSettings.snavLeftWidth === 4 &&
      this.snavLeftWidth > 4) {
      // this.snavLeftWidth = 4;
    }

  }

  //#endregion
}

