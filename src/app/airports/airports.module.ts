import { NgModule } from '@angular/core';
// Local Modules
import { SharedModule } from '../@share/shared.module';
import { AirportsRoutingModule, RoutedComponents } from './routing.module';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        AirportsRoutingModule,
        SharedModule
    ],
    declarations: [
        RoutedComponents,
    ],
    exports: [
    ],
    entryComponents: []
})
export class AirportsModule { }

