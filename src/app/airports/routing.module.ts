import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// ---
// Local Components
import { AppRoutes } from '../@core/const/app-routes.const';
import { HomeComponent } from 'src/app/airports/home.component';
import { AirportsComponent } from './airports.component';


const routes: Routes = [
    { path: '', component: HomeComponent, },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AirportsRoutingModule { }
export const RoutedComponents = [AirportsComponent, HomeComponent];
