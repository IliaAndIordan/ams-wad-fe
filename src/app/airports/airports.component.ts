import { Component, OnInit } from '@angular/core';

@Component({
  template: '<router-outlet></router-outlet>',
})

export class AirportsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
