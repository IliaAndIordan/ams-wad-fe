import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';  // <-- #1 import module

import { MaterialModule } from './material/material.module';
import { ToastrModule } from 'ngx-toastr';
import { ChartsModule } from 'ng2-charts';
import { ModalModule } from './modal/modal.module';
import { CardsModule } from './cards/cards.module';


@NgModule({

    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        ToastrModule.forRoot(),
        ChartsModule,
        ModalModule,
        CardsModule,
    ],
    declarations: [

    ],
    exports: [
        MaterialModule,
        ChartsModule,
        CardsModule,

    ],
    entryComponents: [
    ]
})

export class SharedModule {

    constructor() { }
}

