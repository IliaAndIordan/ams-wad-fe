import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
// Services
import { AuthService } from '../../../@core/auth/api/auth.service';
import { AppRoutes } from '../../../@core/const/app-routes.const';
// Constants

export interface LoginData {
  email: string;
  password: string;
}

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.dialog.html',
  styleUrls: ['./login-modal.dialog.scss']
})

// tslint:disable-next-line:component-class-suffix
export class LoginModalDialog implements OnInit {

  form: FormGroup;

  email: string;
  password: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<LoginModalDialog>,
    @Inject(MAT_DIALOG_DATA) public data: LoginData) {
    this.email = data.email;
    this.password = data.password;
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: [this.email, []],
      password: [this.password, []]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onLoginClick() {

    if (this.form.valid) {
      this.authService.autenticate(
        this.form.value['email'],  this.form.value['password'] )
        .subscribe(user => {
          if (user) {
            // this.spinerService.display(false);

            this.toastr.success('Welcome back ' + user.name + '!', 'Login success');
            setTimeout((router: Router) => {
              this.router.navigate(['/', AppRoutes.Dashboard]);
            }, 1000);
            this.dialogRef.close(user);
          } else {
            // this.spinerService.display(false);
            this.toastr.error('Login Failed', 'Login Failed');
          }
        });
    } else {
      this.toastr.info('Please enter valid values for fields', 'Not valid input');
    }
  }


}
