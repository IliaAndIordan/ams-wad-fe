import { NgModule, Optional, SkipSelf } from '@angular/core';
import { MatDialogModule } from '@angular/material';
import { LoginModalDialog } from './login/login-modal.dialog';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        LoginModalDialog
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        MaterialModule,
    ],
    exports: [
    ],
    entryComponents: [
        LoginModalDialog
    ]
})

export class ModalModule {

    constructor() { }
}
