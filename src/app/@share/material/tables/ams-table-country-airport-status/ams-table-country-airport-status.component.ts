import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { CurrentUserService } from '../../../../@core/auth/current-user.service';
import { AirportEditService } from '../../../../@core/api/airport-edit/airport-edit.service';
import {
  ResponseCountryStateStatusAirport, ICountryStateStatusAirportModel,
  CountryStateStatusAirportModel,
  ResponseAirportCountryStateModel
} from '../../../../@core/api/airport-edit/dto';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-ams-table-country-airport-status',
  templateUrl: './ams-table-country-airport-status.component.html',
  styleUrls: ['./ams-table-country-airport-status.component.scss']
})

export class AmsTableCountryAirportStatusComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  hidePageSize = false;
  resultsLength = 0;
  isLoadingResults = true;
  dataSource: MatTableDataSource<CountryStateStatusAirportModel>;
  public selCountry: CountryStateStatusAirportModel;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered.
   * , 'stateName', 'airportsCount', 'airportsCountAmsStatus0', 'checked'
  **/
  displayedColumns = ['countryIso2', 'countryName', 'stateName', 'airportsCount', 'airportsCountAmsStatus0', 'checked'];

  constructor(private currUserService: CurrentUserService,
    private airportEditService: AirportEditService,
    private breakpointObserver: BreakpointObserver) {

    this.breakpointObserver.observe([
      Breakpoints.Handset,
      Breakpoints.Tablet
    ]).subscribe((state: BreakpointState) => {
      if (state.matches) {
        this.hidePageSize = true;
      } else {
        this.hidePageSize = false;
      }
    });
  }

  ngOnInit() {
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.getCountryStateStatusAirports();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onRowClicked(row) {
    console.log('Row clicked: ', row);
    this.selCountry = row;

    this.airportEditService.getAirportsByCountryState(this.selCountry.countryId, this.selCountry.stateId)
      .subscribe(
        (result: ResponseAirportCountryStateModel) => {
          console.log('onRowClicked result', result);
        },
        error => {
          console.log('onRowClicked error', error);
        });
  }

  isRowSelected(row) {
    return (this.selCountry &&
      (this.selCountry.countryId === row.countryId &&
        this.selCountry.stateId === row.stateId));
  }

  //#region  AirportEditService service

  getCountryStateStatusAirports(): void {
    this.isLoadingResults = true;
    this.airportEditService.getCountryStateStatusAirports()
      .subscribe(
        (result: ResponseCountryStateStatusAirport) => this.processData(result),
        error => {
          this.isLoadingResults = false;
        });

  }

  processData(response: ResponseCountryStateStatusAirport): void {
    console.log('processData', response);

    const data: ICountryStateStatusAirportModel[] = response.data;
    const casData = Array<CountryStateStatusAirportModel>();
    if (data) {
      data.forEach(element => {
        const item: CountryStateStatusAirportModel = CountryStateStatusAirportModel.fromJSON(element);
        casData.push(item);
      });
      console.log('countryId: ' + this.airportEditService.countryId +
        ', stateId: ' + this.airportEditService.stateId);
      if (this.airportEditService.countryId && this.airportEditService.stateId) {

        const filter = casData.filter(x => x.countryId === this.airportEditService.countryId &&
          x.stateId === this.airportEditService.stateId);

        if (filter && filter.length > 0) {
          this.selCountry = filter[0];
        }
      }
    }
    console.log('selCountry: ', this.selCountry);
    this.dataSource = new MatTableDataSource(casData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.resultsLength = this.dataSource.data.length;
    this.isLoadingResults = false;
  }

  //#endregion
}
