import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CurrentUserService } from '../../../../@core/auth/current-user.service';
import { UserModel } from '../../../../@core/auth/api/dto';

@Component({
  selector: 'app-ams-lnav-item-profile',
  templateUrl: './ams-lnav-item-profile.component.html',
  styleUrls: ['./ams-lnav-item-profile.component.scss']
})
export class AmsLnavItemProfileComponent implements OnInit {

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClick: EventEmitter<any> = new EventEmitter<any>();

  public user: UserModel;
  private userChangedSubscription: any;

  avatarImageUrl: string;

  constructor(private currUserService: CurrentUserService) {
    this.user = this.currUserService.user;
    this.avatarImageUrl = this.currUserService.avatarUrl;
    this.userChangedSubscription = this.currUserService.UserChanged
      .subscribe(newUser => {
        this.user = newUser;
        this.avatarImageUrl = this.currUserService.avatarUrl;
      });
  }

  ngOnInit() {
  }

  public get title(): string {
    let rv = 'Login';
    if (this.user) {
      rv = this.user.name ? this.user.name : this.user.eMail;
    }
    return rv;
  }

  handleClick(event: any) {
    // what we sending to the event handler that is being passed to parent function
    this.onClick.emit('emit');
  }

}
