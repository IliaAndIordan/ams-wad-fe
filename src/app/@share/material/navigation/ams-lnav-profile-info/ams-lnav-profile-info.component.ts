import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from '../../../../@core/auth/current-user.service';
import { UserModel } from '../../../../@core/auth/api/dto';
import { LocalUserSettings } from '../../../../@core/auth/local-user-settings';

@Component({
  selector: 'app-ams-lnav-profile-info',
  templateUrl: './ams-lnav-profile-info.component.html',
  styleUrls: ['./ams-lnav-profile-info.component.scss']
})
export class AmsLnavProfileInfoComponent implements OnInit {

  public snavLeftWidth = 4;
  public currUser: UserModel;
  public paddingBottom = 0;
  public maxHeight = 64;
  public paddingTop = 12;
  public heightPx = 64;
  public minHeight = 64;

  constructor(private currUserService: CurrentUserService) {

    this.snavLeftWidth = this.currUserService.localSettings.snavLeftWidth;
    this.currUser = this.currUserService.user;

    this.currUserService.UserChanged.subscribe((user: UserModel) => {
      this.onUserChanged();
    });

    this.currUserService.LocalSettingsChanged.subscribe((settings: LocalUserSettings) => {
      this.onSettingsChanged();
    });
    this.onSettingsChanged();
  }

  ngOnInit() {
  }

  onUserChanged() {
    this.currUser = this.currUserService.user;
  }

  onSettingsChanged() {
    this.snavLeftWidth = this.currUserService.localSettings.snavLeftWidth;
    this.paddingBottom = this.snavLeftWidth > 6 ? 64 : 0;
    this.maxHeight = this.snavLeftWidth > 6 ? 136 : 64;
    this.paddingTop = this.snavLeftWidth > 6 ? 24 : 12;
    this.heightPx = this.snavLeftWidth > 6 ? 136 : 64;
    this.minHeight = this.snavLeftWidth > 6 ? 136 : 64;

  }

}
