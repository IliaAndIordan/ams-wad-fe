import { Component, Output, EventEmitter } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-ams-nav-toolbar',
  templateUrl: './ams-nav-toolbar.component.html',
  styleUrls: ['./ams-nav-toolbar.component.css']
})
export class AmsNavToolbarComponent {

  //#region Bindings

  @Output() snavLeftTogggle: EventEmitter<any> = new EventEmitter<any>();
  @Output() snavRightogggle: EventEmitter<any> = new EventEmitter<any>();

  //#endregion

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver) { }

  handleSNavLeftToggle() {
    this.snavLeftTogggle.emit();
  }

  handleSNavRightToggle() {
    this.snavRightogggle.emit();
  }
}
