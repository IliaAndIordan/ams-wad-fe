import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SnavLeftComponent } from './snav-left/snav-left.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { AmsNavToolbarComponent } from './ams-nav-toolbar/ams-nav-toolbar.component';
import { AmsLnavItemProfileComponent } from './ams-lnav-item-profile/ams-lnav-item-profile.component';
import { AmsLnavListItemsComponent } from './ams-lnav-list-items/ams-lnav-list-items.component';
import { RouterModule } from '@angular/router';
import { AmsLnavProfileInfoComponent } from './ams-lnav-profile-info/ams-lnav-profile-info.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  declarations: [
    SnavLeftComponent,
    AmsNavToolbarComponent,
    AmsLnavItemProfileComponent,
    AmsLnavListItemsComponent,
    AmsLnavProfileInfoComponent,
  ],
  exports: [
    SnavLeftComponent,
    AmsNavToolbarComponent,
    AmsLnavItemProfileComponent,
    AmsLnavListItemsComponent,
    AmsLnavProfileInfoComponent,
  ],
})
export class NavigationModule { }
