
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSidenavModule } from '@angular/material/sidenav';
import { SnavLeftComponent } from './snav-left.component';

describe('SnavLeftComponent', () => {
  let component: SnavLeftComponent;
  let fixture: ComponentFixture<SnavLeftComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      imports: [MatSidenavModule],
      declarations: [SnavLeftComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SnavLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
