import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUserService } from '../../../../@core/auth/current-user.service';
// -- Constants
import { AMS_WAD_LEFT_NAVIGATION } from '../../../../@core/const/ams-wad-lnav.const';
import { UserModel } from '../../../../@core/auth/api/dto';
import { AuthService } from '../../../../@core/auth/api/auth.service';


@Component({
  selector: 'app-ams-lnav-list-items',
  templateUrl: './ams-lnav-list-items.component.html',
  styleUrls: ['./ams-lnav-list-items.component.scss']
})
export class AmsLnavListItemsComponent implements OnInit {
  public navigation = AMS_WAD_LEFT_NAVIGATION;
  public user: UserModel;
  public isAuthenticated: boolean;

  constructor(private router: Router,
    private currUserService: CurrentUserService,
    private route: ActivatedRoute,
    private authService: AuthService) {

    this.user = this.currUserService.user;
    this.isAuthenticated = this.currUserService.isAuthenticated;

    this.currUserService.UserChanged.subscribe((user: UserModel) => {
      this.onUserChanged();
    });
  }

  ngOnInit() {
  }

  public isVisible(item) {
    let visible = true;
    if (item.authorised !== 'undefined') {
      visible = (item.authorised === this.currUserService.isAuthenticated);
    }

    return visible;
  }

  public onUserChanged(): void {
    this.user = this.currUserService.user;
    this.isAuthenticated = this.currUserService.isAuthenticated;
    // console.log('app-ams-lnav-list-items.onUserChanged: isAuthenticated=', this.isAuthenticated);
  }

  logout(): void {
    this.authService.logout();
  }
}
