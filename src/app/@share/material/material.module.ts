import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

// Materisl Modules
import {
  MatButtonModule, MatCheckboxModule, MatSidenavModule,
  MatMenuModule, MatToolbarModule, MatIconModule, MatListModule,
  MatFormFieldModule, MatInputModule, MatRadioModule, MatTreeModule,
  MatCardModule, MatTableModule, MatExpansionModule, MatGridListModule,
  MatPaginatorModule, MatSortModule, MatProgressSpinnerModule, MatProgressBarModule
} from '@angular/material';
import { NavigationModule } from './navigation/navigation.module';
import { LayoutModule } from '@angular/cdk/layout';
// tslint:disable-next-line:max-line-length
import { AmsTableCountryAirportStatusComponent } from './tables/ams-table-country-airport-status/ams-table-country-airport-status.component';
import { BooleanPipe } from '../pipes/boolean.pipe';
import { TruncatePipe } from '../pipes/truncate.pipe';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,

    // ---Material modules
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatTreeModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    // --- Custome Modules
    NavigationModule,
    MatPaginatorModule,
    MatSortModule,

  ],
  declarations: [
    TruncatePipe,
    BooleanPipe,
    AmsTableCountryAirportStatusComponent
  ],
  exports: [
    // ---Material modules
    FlexLayoutModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatTreeModule,
    MatCardModule,
    MatTableModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatGridListModule,
    LayoutModule,
    // --- Custome Modules
    NavigationModule,
    TruncatePipe,
    BooleanPipe,
    // --- Components
    AmsTableCountryAirportStatusComponent,
  ],

})
export class MaterialModule { }
