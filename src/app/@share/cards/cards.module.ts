import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { ToastrModule } from 'ngx-toastr';
import { ChartsModule } from 'ng2-charts';
import { ModalModule } from '../modal/modal.module';
import { AmsCardCountryApStatComponent } from './ams-card-country-ap-stat/ams-card-country-ap-stat.component';


@NgModule({

    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        ToastrModule.forRoot(),
        ChartsModule,
        ModalModule,
    ],
    declarations: [
        AmsCardCountryApStatComponent,
    ],
    exports: [
        AmsCardCountryApStatComponent,
    ],
    entryComponents: [
    ]
})

export class CardsModule {

    constructor() { }
}
