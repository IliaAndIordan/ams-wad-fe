import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-ams-card-country-ap-stat',
  templateUrl: './ams-card-country-ap-stat.component.html',
  styleUrls: ['./ams-card-country-ap-stat.component.scss']
})
export class AmsCardCountryApStatComponent implements OnInit {

  constructor(private breakpointObserver: BreakpointObserver) {

    this.breakpointObserver.observe([
      Breakpoints.Handset,
      Breakpoints.Tablet
    ]).subscribe((state: BreakpointState) => {
      console.log('BreakpointState:', state);
    });
  }

  ngOnInit() {
  }

}
