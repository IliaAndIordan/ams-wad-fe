import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointState, BreakpointObserver } from '@angular/cdk/layout';
import { Chart } from 'chart.js';

@Component({
  templateUrl: './home.component.html',
  // styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  airportCityAssigned = 249;
  progressAirportCity = (this.airportCityAssigned / 45991);
  progressAirportCityPct = (this.progressAirportCity * 100);
  minichartOneData: Array<any> = [
    {
      data: [35, 31, 12, 51, 39, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: ' ',
      bars: {
        show: true,
        barWidth: 0.5,
        barSpacing: 2,
        order: 0,
        lineWidth: 0,
        fillColor: '#fff'
      }
    }];
  // tslint:disable-next-line:max-line-length
  minichartOneLabels: Array<any> = ['22 Sep 2018', '18 Oct 2018', '20 Oct 2018', '25 Oct 2018', '27 Oct 2018', '07 Nov 2018', 'Mart', 'Apr', 'May', 'June', 'July'];
  minichartTwoData: Array<any> = [
    {
      data: [49504856.16, 81418863.3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: ' ',
      bars: {
        show: true,
        barWidth: 0.5,
        barSpacing: 2,
        order: 0,
        lineWidth: 0,
        fillColor: '#fff'
      }
    }];
  minichartTwoLabels: Array<any> = ['19 Oct', '26 Oct', 'November', 'December', 'Jan', 'Feb', 'Mart', 'Apr', 'May', 'June', 'July'];
 aeCurrentValue = 81418863.3;


  minichartOneColors = [
    { // 1st Year.
      backgroundColor: '#fff',
      borderColor: 'rgba(227, 227, 227,.80)',
    },
    { // 2nd Year.
      backgroundColor: 'rgba(30, 169, 224, 0.8)'
    }
  ];
  miniChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
      bodyFontSize: 9,
      titleFontSize: 9,
      position: 'average'
    },
    scales: {
      xAxes: [{
        display: false,
        barPercentage: 0.7,
      }],
      yAxes: [{
        display: false,
        stacked: false,
        ticks: {
          display: false,
          /*min: 0 - 5,
          max: 100 + 5,*/
        }
      }],
    },
    elements: {
      line: {
        borderWidth: 1
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
      },
    },
    legend: {
      display: false
    }
  };

  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Card 1', cols: 1, rows: 1 },
          { title: 'Card 2', cols: 1, rows: 1 },
          { title: 'Card 3', cols: 1, rows: 1 },
          { title: 'Card 4', cols: 1, rows: 1 }
        ];
      }

      return [
        { title: 'Card 1', cols: 2, rows: 1 },
        { title: 'Card 2', cols: 1, rows: 1 },
        { title: 'Card 3', cols: 1, rows: 2 },
        { title: 'Card 4', cols: 1, rows: 1 }
      ];
    })
  );

  //#region Chart
  public apCheckRequestedData: Array<any> = [
    {
      data: [45991, 59, 84, 84, 51, 55, 40],
      label: 'Initial check is done'
    }
  ];
  public apCheckDoneData: Array<any> = [
    {
      data: [492, 59, 84, 84, 51, 55, 40],
      label: 'Initial check is done'
    }
  ];

  public lineChartData: Array<any> = [
    {
      data: [35, 31, 12, 51, 39, 81, 0], label: 'Airport City assignment',
      lines: { show: true, fill: 0.98 }, stack: true
    },
    {
      data: [35, 66, 78, 129, 168, 249, 249], label: 'IAirport City assigned',
      lines: { show: true, fill: 0.98 }, stack: true,
    },
    {
      data: [35, 31, 12, 51, 0, 0, 0], label: 'Initial check requested',
      lines: { show: true, fill: 0.98 }, stack: true
    },

  ];
  public lineChartLabels: Array<any> = ['22 Sep 2018', '18 Oct 2018', '20 Oct 2018', '25 Oct 2018', '07 Nov 2018', 'June', 'July'];
  public lineChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent'
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        }

      }],
      yAxes: [{
        display: false,
        stacked: false,
        ticks: {
          display: false,
          /*min: 0 - 5,
          max: 100 + 5,*/
        }
      }],
    },
    elements: {
      line: {
        borderWidth: 2
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
      },
    },
    legend: {
      display: false
    }
  };
  public lineChartColors: Array<any> = [
    { // yelow
      backgroundColor: 'rgba(241, 221, 44,0.4)',
      borderColor: 'rgba(241, 221, 44,.50)',
    },
    { // light grey
      backgroundColor: 'rgba(227, 227, 227,0.2)',
      borderColor: 'rgba(227, 227, 227,.50)',
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.4)',
      borderColor: 'rgba(148,159,177,.50)',
    },

  ];
  public lineChartLegend = true;
  public lineChartType = 'line';

  public randomize(): void {
    const _lineChartData: Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = { data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label };
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  //#endregion


  /** Based on the screen size, switch from standard to one column per row */


  constructor(private breakpointObserver: BreakpointObserver) {

  }

  ngOnInit(): void {

    // this.minichartOneData = [this.lineChartData[0]];
    // this.minichartOneLabels = this.lineChartLabels;

  }

  ngAfterViewInit(): void {

  }

}
