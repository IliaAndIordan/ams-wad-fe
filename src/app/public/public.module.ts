import { NgModule } from '@angular/core';
// Local Modules
import { SharedModule } from '../@share/shared.module';
import { PublicRoutingModule, RoutedComponents } from './routing.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        CommonModule,
        PublicRoutingModule,
        SharedModule
    ],
    declarations: [
        RoutedComponents,
    ],
    exports: [
    ],
    entryComponents: []
})
export class PublicModule { }

