import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// --- Constants
import { AppRoutes } from '../@core/const/app-routes.const';
import { AuthGuard } from '../@core/guards/aut-guard.service';
// Local Components
import { NewspaperComponent } from './newspaper.component';
import { HomeComponent } from './home.component';

const routes: Routes = [
    {path: '', component: HomeComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NewspaperRoutingModule { }

export const RoutedComponents = [NewspaperComponent, HomeComponent];
