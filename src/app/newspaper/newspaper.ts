export class NewspaperModel {
    public head: NewspaperHeadModel;
    public breakingNews: Array<BreakingNewsModel>;
}

export class NewspaperHeadModel {
    public name: string;
    public id: number;
    public adate: Date;
}

export class BreakingNewsModel {
    public url: string;
    public imageUrl: string;
    public title: string;
    public contentHtml: string;
    public maxLenght = 60;
}
